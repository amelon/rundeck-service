# rundeck-service

* Source: https://bitbucket.org/amelon/rundeck-service/
* Reference: https://docs.rundeck.com/docs/administration/configuration/docker.html

## Prerequisites

The Rundeck service creates multiple Docker containers. To allow the containers to write to the volumes on the host server, the user account used to run the Rundeck service must share the same UID/GID with the containers.

* The `rundeck` container has a `rundeck` user with UID 1000.
* The `db` container has a `mysql` user with UID 1000 and group `mysql` with GID 1000.

On the host server, you need to create a user account which has UID 1000 and GID 1000. The following can be used:

    $ sudo useradd -m -s /bin/bash -u 1000 -g 1000 iamdocker
    $ sudo echo "iamdocker ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/iamdocker
    $ sudo chmod 440 /etc/sudoers.d/iamdocker

## Configure Dockerfile

Edit `Dockerfile`:

* Locate `RUNDECK_GRAILS_URL=http://127.0.0.1:4440` and change `127.0.0.1` to your server IP
* Change `RUNDECK_MAIL_FROM`
* Change `RUNDECK_MAIL_SMTP_HOST`
* Change `RUNDECK_DATABASE_USERNAME`
* Change `RUNDECK_DATABASE_PASSWORD`
* Change `MARIADB_USER`
* Change `MARIADB_PASSWORD`

## Manage Nagios Docker container

Build the Docker image:

    $ docker-compose build

Bring up the containers:

    $ docker-compose up -d

Verify that the containers are up and running (State is Up):

    $ docker-compose ps

Check the log files:

    $ docker-compose logs -f

Bring down the comtainers:

    $ docker-compose down

## Verify web connectivities

* Check Adminer UI at http://<server_ip>:10008/ (Login: `rundeck`, password: `rundeck`)
* Check Rundeck UI at http://<server_ip>:4440/  (Login: `admin`, password: `admin`)

## Reset services

To reset the Rundeck service for whatever reason, you need to remove the databases.

Stop the service.

    $ docker-compose down

Remove the databases. Note: This step is irreversible so be sure that this is what you want to do.

    $ cd rundeck-service
    $ rm -rf data/db/*

Start the service up again.

    $ docker-compose up -d

## Rundeck configuration

### Configure a node source

Go to "Project Settings" ==> "Add a new Node Source" ==> "Directory" ==> "Directory path", specify `/home/rundeck/server/nodes`.

Go to "Nodes" and click on "Filters: All Nodes" to see which nodes are detected. To add a new node, use `/home/rundeck/server/nodes/my-minion1.json` as the template.

### Configure public/private keys and passphrase to SSH to nodes

On your local system, generate the public/private keys.

    $ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/iamdocker/.ssh/id_rsa): ./id_rsa
    Enter passphrase (empty for no passphrase): **********
    Enter same passphrase again: **********
    Your identification has been saved in ./id_rsa.
    Your public key has been saved in ./id_rsa.pub.
    The key fingerprint is:
    SHA256:2AAkX/JAM/7Ayz6du5K8fkYp4/lkHzx6s7B9gdBAMdI iamdocker@amelon-mbp.lan
    The key's randomart image is:
    +---[RSA 3072]----+
    |  .oXo=.         |
    |   = XE.         |
    |    = oo         |
    |   . +.+.        |
    |    o ooS.       |
    |   .o.oo. .      |
    |   oo** +  .     |
    |    *+oBoo.      |
    |   .o**o++       |
    +----[SHA256]-----+

Save the passphrase to `id_rsa.passprhase`. In the following example, assume my passphrase is `mypassphrase123`.

    $ echo "mypassphrase123" > id_rsa.passphrase

    $ ls -la
    total 16
    drwxr-xr-x    4 iamdocker  iamdocker    128 Oct  5 00:03 .
    drwx------@ 603 iamdocker  iamdocker  19296 Oct  5 00:02 ..
    -rw-------    1 iamdocker  iamdocker   2655 Oct  5 00:03 id_rsa
    -rw-r--r--    1 iamdocker  iamdocker     16 Oct  5 00:05 id_rsa.passphrase
    -rw-r--r--    1 iamdocker  iamdocker    572 Oct  5 00:03 id_rsa.pub

The `id_rsa` file is in OPENSSH format. Convert it to PEM format.

    $ cat id_rsa
    -----BEGIN OPENSSH PRIVATE KEY-----
    b3BlbnNzaC1rZXktdjEAAAAACmFlczI1Ni1jdHIAAAAGYmNyeXB0AAAAGAAAABBMrIkY6K
    ...
    1RbBVMOpIstigj56gRWVxsBIi8c=
    -----END OPENSSH PRIVATE KEY-----

    $ ssh-keygen -p -m PEM -f ./id_rsa
    Enter old passphrase: **********
    Key has comment 'iamdocker@amelon-mbp.lan'
    Enter new passphrase (empty for no passphrase): **********
    Enter same passphrase again: **********
    Your identification has been saved with the new passphrase.

    $ cat id_rsa
    -----BEGIN RSA PRIVATE KEY-----
    Proc-Type: 4,ENCRYPTED
    DEK-Info: AES-128-CBC,E458ED94999610222D88F82C7E024EAE

    B+WEnTgM8gziaMm2gzVl3/uT+3dbCezowSeVO4CY2HOyi2fL0v+Vq5Ksh/CAhAP5
    ...
    oOiYBa4Y/xCSt4EkG390/Ryp1UzPTj+a4ZJgQbpqSU8OX4trcdXY0JthadeoS2D7
    -----END RSA PRIVATE KEY-----

Save these public/private keys in a secure location for future reference. Now upload them to Rundeck's Key Storage.

* "Project Settings" ==> "Key Storage" ==> "Add or Upload a Key"

    * Upload `id_rsa` as a Private Key.
    * Upload `id_rsa.pub` as a Public Key.
    * Upload `id_rsa.passprhase` as a Password.

Configure SSH keys for project

* "Project Settings" ==> "Default Node Executor"

    * "SSH Key File path" - leave this blank
    * "SSH Key Storage Path" - `keys/project/TestProject/id_rsa`
    * "SSH Password Storage Path" - leave this blank
    * "SSH Key Passphrase Storage Path" - `keys/project/TestProject/id_rsa.passphrase`
    * "SSH Authentication" - privateKey
    * Click "Save"

Now test "Commands" ==> Select a node ==> Enter a command. It should work.


